from conan import ConanFile

class TuneMeasurementRecipe(ConanFile):
    name = "tunemeasurement"
    executable = "ds_TuneMeasurement"
    version = "1.3.1"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Nicolas Leclercq"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/beamdiagnostics/tunemeasurement.git"
    description = "TuneMeasurement device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("fftw/[>=1.0]@soleil/stable")
        self.requires("fft/[>=1.0]@soleil/stable")
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("utils/[>=1.0]@soleil/stable")
        self.requires("boostlibraries/1.65.1a@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
