#ifndef _TUNETASK_H_
#define _TUNETASK_H_

#include <boost/smart_ptr.hpp>

#include <yat/threading/Task.h>
#include <yat/memory/DataBuffer.h>
#include <yat4tango/ExceptionHelper.h>

// FFT headers
#include "WindowInfo.hpp"
#include "FFTProcessor.hpp"
#include "FFTProcessorConfig.hpp"
#include "RMSPowerAveragedSpectrumProcessor.hpp"
#include "PowerSpectrumUnitConverter.hpp"
#include "PhaseProcessor.hpp" // for phase computation


namespace TuneMeasurement_ns
{

  struct TuneProperties
  {
    TuneProperties ();

    TuneProperties (const TuneProperties& src)
    {
      *this = src;
    }

    const TuneProperties & operator= (const TuneProperties& src)
    {
      if (this == &src)
        return *this;
      src_device = src.src_device;
      src_data_attr_name = src.src_data_attr_name;
      src_size_attr_name = src.src_size_attr_name; 
      src_size_write_enabled = src.src_size_write_enabled;
      auto_start = src.auto_start;
      peaks_search_device = src.peaks_search_device;
      return *this;
    }

    std::string src_device;
    std::string src_data_attr_name;
    std::string src_size_attr_name;
    bool        src_size_write_enabled;
    bool        auto_start;
    std::string peaks_search_device;
  };

  struct TuneConfig
  {
    TuneConfig();

    TuneConfig (const TuneConfig& src)
    {
      *this = src;
    }

    const TuneConfig & operator= (const TuneConfig& src)
    {
      if (this == &src)
        return *this;
      properties = src.properties;
      fft_window_size = src.fft_window_size;
      nu_search_start = src.nu_search_start;
      nu_search_end = src.nu_search_end;
      fft_averaging = src.fft_averaging;
      skipped_first_samples = src.skipped_first_samples;
      window_type = src.window_type;
      minimal_nb_points = src.minimal_nb_points;
      comput_period = src.comput_period;
      nu_in_higher_interval = src.nu_in_higher_interval;
      //- CTRLRFC-1178
      select_upper_peak = src.select_upper_peak;
      switch_search_two_peak = src.switch_search_two_peak;

      return *this;
    }

    TuneProperties    properties;
    Tango::DevLong    fft_window_size;
    Tango::DevDouble  nu_search_start;
    Tango::DevDouble  nu_search_end;
    Tango::DevUShort  fft_averaging;
    Tango::DevUShort  skipped_first_samples;
    FFT::WindowType   window_type;
    Tango::DevLong    minimal_nb_points;
    Tango::DevLong    comput_period;
    Tango::DevBoolean nu_in_higher_interval;
      //- CTRLRFC-1178
    Tango::DevBoolean select_upper_peak;
    Tango::DevBoolean switch_search_two_peak;
  };    

  typedef yat::Buffer<Tango::DevDouble> Buffer;

  struct TuneData
  {
    TuneData ();
    
    TuneData(const TuneData& src)
    {
      *this = src;
    }

    const TuneData & operator= (const TuneData& src)
    {
      if (this == &src)
        return *this;

      config = src.config;
      valid = src.valid;
      fft_abs = src.fft_abs;
      fft_ord = src.fft_ord;
      fft_phase = src.fft_phase;
      fft_nb_bins = src.fft_nb_bins;
      fft_size = src.fft_size;
      nu_raw = src.nu_raw;
      nu = src.nu;
      fft_max = src.fft_max;
      estim_comput_time = src.estim_comput_time;
      //- CTRLRFC-1178
      upper_peak_nu = src.upper_peak_nu;
      lower_peak_nu = src.lower_peak_nu;
      return *this;
    }

    TuneConfig        config;

    bool              valid;
    Buffer            fft_abs;
    Buffer            fft_ord;
    Buffer            fft_phase;
    Tango::DevLong    fft_nb_bins; // the number of bins in fft_ord and fft_abs. should be 'fft_size_/2 + 1'
    Tango::DevLong    fft_size;  // the number of points on which the FFT is computed
    Tango::DevDouble  nu_raw;
    Tango::DevDouble  nu;
    Tango::DevDouble  fft_max;
    Tango::DevLong    estim_comput_time;
    //- CTRLRFC-1178
    Tango::DevDouble  upper_peak_nu;
    Tango::DevDouble  lower_peak_nu;
  };

  typedef boost::shared_ptr<TuneData> TuneDataP;

  class TuneTask : public yat::Task,
                   public Tango::LogAdapter
  {
    public:
      TuneTask( const TuneConfig& config,
                Tango::DeviceImpl* device );

      ~TuneTask();

      void start( void );

      void stop( void );

      void configure( const TuneConfig& config );

      TuneDataP get_data( void );

      void get_data(TuneData & dest);

      void get_state_status( Tango::DevState& state, std::string& status );

    protected: //- [yat::Task impl]
      virtual void handle_message( yat::Message& msg )
        throw (yat::Exception);

    private:
      void configure_i();

      void process_i();

      void set_state_status( Tango::DevState state, std::string status );

      void set_error_status( Tango::DevFailed& ex );
     
      void set_error_status( yat::Exception& ex );
      
      void set_data( TuneDataP data );
      
      void set_empty_data( void );

    private:
      TuneConfig config_;

      yat::Mutex mutex_;
      TuneDataP  data_;

      Tango::DevState state_;
      std::string     status_;

      boost::shared_ptr<Tango::DeviceProxy>   dev_proxy_;
      Tango::AttributeInfoEx data_attr_info_;
      
      boost::shared_ptr<Tango::DeviceProxy>   peaks_proxy_;

      FFT::FFTProcessor                       fft_proc_;
      FFT::FFTProcessorConfig                 fft_proc_config_;
      FFT::RMSPowerAveragedSpectrumProcessor  fft_average_proc_;
      FFT::PowerSpectrumUnitConverter         fft_unit_converter_;
      FFT::PhaseProcessor                     fft_phase_proc_;
      Buffer                                  fft_input_data_;
  };

  typedef boost::shared_ptr<TuneTask> TuneTaskP;


  struct TuneTaskExiter
  {
    void operator() ( TuneTask* t )
    {
      try
      {
        t->exit();
      }
      catch(...)
      {
        // ignore error
      }
    }
  };

}
#endif
