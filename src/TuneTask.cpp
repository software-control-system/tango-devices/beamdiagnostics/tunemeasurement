#include <math.h>
#include "TuneTask.h"
//#include "PeakFinder.h"
#include <yat/time/Time.h>
#include <yat/Portability.h>    //- NaN

namespace TuneMeasurement_ns
{

  const size_t START_MSG = yat::FIRST_USER_MSG;
  const size_t STOP_MSG  = yat::FIRST_USER_MSG + 1;
  const size_t CONFIGURE_MSG  = yat::FIRST_USER_MSG + 2;

  const std::string STANDBY_STATUS( "Waiting for Start..." );
  const std::string RUNNING_STATUS( "Tune Measurement running" );
  const std::string FAULT_STATUS  ( "A fatal error occurred" );

  // const double kNaN = ::sqrt(-1.0);

  TuneProperties::TuneProperties()
    : src_size_write_enabled(false),
      auto_start(true)
  {
  }

  TuneConfig::TuneConfig()
    : fft_window_size(512),
      nu_search_start(0),
      nu_search_end(0.5),
      fft_averaging(1),
      skipped_first_samples(0),
      window_type(FFT::HANN),
      minimal_nb_points(512),
      comput_period(500),
      nu_in_higher_interval(false),
      select_upper_peak(false),
      switch_search_two_peak(false)
      //prominence(0.)
  {
  }

  TuneData::TuneData()
    : valid(false),
      fft_nb_bins(0),
      fft_size(0),
      nu_raw(yat::IEEE_NAN),
      nu(yat::IEEE_NAN),
      fft_max(0),
      estim_comput_time(0),
      upper_peak_nu(yat::IEEE_NAN),
      lower_peak_nu(yat::IEEE_NAN)
  {
  }


  TuneTask::TuneTask( const TuneConfig& config, Tango::DeviceImpl* device )
    : Tango::LogAdapter(device),
      config_(config),
      state_( Tango::INIT ),
      status_( "Initialisation..." ),
      fft_average_proc_(FFT::MOVING_AVERAGE)
  {
    this->go(5000);
    this->set_empty_data();
  }

  TuneTask::~TuneTask()
  {
  }

  void TuneTask::start( void )
  {
    yat::Message* msg = yat::Message::allocate( START_MSG, INIT_MSG_PRIORITY, false );
    this->post( msg );
  }

  void TuneTask::stop( void )
  {
    yat::Message* msg = yat::Message::allocate( STOP_MSG, INIT_MSG_PRIORITY, false );
    this->post( msg );
  }

  void TuneTask::configure( const TuneConfig& config )
  {
    yat::Message* msg = yat::Message::allocate( CONFIGURE_MSG, INIT_MSG_PRIORITY, false );
    msg->attach_data( config );
    this->post( msg );
  }

  void TuneTask::get_data(TuneData & dest)
  {

    yat::AutoMutex<> lock(mutex_);
    dest = *data_;
  }

  TuneDataP TuneTask::get_data()
  {

    yat::AutoMutex<> lock(mutex_);
    return data_;
  }

  void TuneTask::set_data(TuneDataP data)
  {
    yat::AutoMutex<> lock(mutex_);
    data_ = data;
  }

  void TuneTask::set_empty_data(void)
  {
    TuneDataP empty_data( new TuneData() );
    this->set_data( empty_data );
  }

  void TuneTask::get_state_status( Tango::DevState& state, std::string& status )
  {
    yat::AutoMutex<> lock(mutex_);
    state  = state_;
    status = status_;
  }

  void TuneTask::set_state_status( Tango::DevState state, std::string status )
  {
    yat::AutoMutex<> lock(mutex_);
    state_  = state;
    status_ = status;
  }

  void TuneTask::set_error_status( yat::Exception& ex )
  {
    std::ostringstream oss;
    oss << "Fatal Error received" << std::endl
        << std::endl
        << "Error stack :" << std::endl;

    const yat::Exception::ErrorList& yat_errors = ex.errors;
    for (size_t i = 0; i < yat_errors.size(); i++)
    {
      oss << "[ " << yat_errors[i].reason << " at " << yat_errors[i].origin.c_str() << std::endl
          << "  " << yat_errors[i].desc << " ]" << std::endl;
    }

    this->set_state_status( Tango::FAULT, oss.str() );
  }

  void TuneTask::set_error_status( Tango::DevFailed& ex )
  {
    std::ostringstream oss;
    oss << "Fatal Error received" << std::endl
        << std::endl
        << "Error stack :" << std::endl;

    const Tango::DevErrorList& errors = ex.errors;
    for (size_t i = 0; i < errors.length(); i++)
    {
      oss << "[ " << errors[i].reason << " at " << errors[i].origin << std::endl
          << "  " << errors[i].desc << " ]" << std::endl;
    }

    this->set_state_status( Tango::FAULT, oss.str() );
  }


  void TuneTask::handle_message( yat::Message& msg )
    throw (yat::Exception)
  {
    try
    {
      switch ( msg.type() )
      {
      case yat::TASK_INIT:
        {
          //- data src
          dev_proxy_.reset( new Tango::DeviceProxy( config_.properties.src_device ) );
          dev_proxy_->set_timeout_millis( 20000 );
          dev_proxy_->ping();
          
          if ( !config_.properties.peaks_search_device.empty() )
          {
            //- peaks search device proxy
            peaks_proxy_.reset( new Tango::DeviceProxy( config_.properties.peaks_search_device ) );
          }

          data_.reset( new TuneData() );

          data_attr_info_ = dev_proxy_->attribute_query( config_.properties.src_data_attr_name );

          //- initial configuration
          this->configure_i();

          this->set_state_status( Tango::STANDBY, STANDBY_STATUS );

          if ( config_.properties.auto_start )
          {
            this->enable_periodic_msg( true );
            this->set_state_status( Tango::RUNNING, RUNNING_STATUS );
          }
        }
        break;
      case yat::TASK_EXIT:
        {
        }
        break;
      case yat::TASK_PERIODIC:
        {
          this->process_i();
        }
        break;
      case START_MSG:
        {
          this->enable_periodic_msg( true );
          this->configure_i(); // force reconfiguration
          this->set_state_status( Tango::RUNNING, RUNNING_STATUS );
        }
        break;
      case STOP_MSG:
        {
          this->enable_periodic_msg( false );
          this->set_state_status( Tango::STANDBY, STANDBY_STATUS );
        }
        break;
      case CONFIGURE_MSG:
        {
          TuneConfig* conf_ptr = 0;
          msg.detach_data( conf_ptr );
          this->config_ = *conf_ptr;
          delete conf_ptr;

          this->configure_i();
        }
        break;
      }
    }
    catch( std::exception& ex )
    {
      this->set_state_status( Tango::FAULT, ex.what() );
      this->enable_periodic_msg( false );
      this->set_empty_data();
    }
    catch( yat::Exception& ex )
    {
      this->set_error_status( ex );
      this->enable_periodic_msg( false );
      this->set_empty_data();
    }
    catch( Tango::DevFailed& df )
    {
      this->set_error_status( df );
      // When connection problem to data source, do not interrupt task to have a transparent reconnection to data source
      // JIRA TANGODEVIC-373
 //     this->enable_periodic_msg( false );
      this->set_empty_data();
    }
    catch( ... )
    {
      this->set_state_status( Tango::FAULT, "Unknown error received" );
      this->enable_periodic_msg( false );
      this->set_empty_data();
    }
  }


  void TuneTask::configure_i()
  {
    this->set_periodic_msg_period( config_.comput_period );

    double resolution = 0.;
    if (config_.properties.src_size_write_enabled)
    {
      resolution = 1.0 / static_cast<double>(config_.fft_window_size);
    }
    else
    {
      Tango::DeviceAttribute dev_attr = dev_proxy_->read_attribute( config_.properties.src_size_attr_name );
      long n;
      dev_attr >> n;
      resolution = 1.0 / static_cast<double>(n);
      config_.fft_window_size = n;
    }

/*    if ( peaks_proxy_ )
    {
      //- write PeaksSearch "prominence"
      Tango::DeviceAttribute attr_in("Prominence", config_.prominence);

      peaks_proxy_->write_attribute(attr_in);
    }
*/
    //- init configuration
    fft_proc_config_.init(1.0,                      // sampling freq
                          resolution,               // resolution bandwidth
                          config_.window_type,      // window type
                          0.0,                      // overlap percentage
                          config_.minimal_nb_points // minimal number of points in the FFT
                         );
    INFO_STREAM << "\tFFT window size : " << fft_proc_config_.getWindowSize() << ENDLOG;
    INFO_STREAM << "\tFFT size        : " << fft_proc_config_.getFFTSize() << ENDLOG;
    INFO_STREAM << "\tFFT resolution  : " << fft_proc_config_.getResolution() << ENDLOG;

    //- init FFT processor
//-XE    fft_proc_.reset(); //- Already done in setConfig!
    fft_proc_.setConfig( fft_proc_config_ );

    //- init Average processor
   fft_average_proc_.init(config_.fft_averaging);
   fft_average_proc_.restart();

    //- init Unit converter
    fft_unit_converter_.init( FFT::VOLT_SQUARE,
                              0.0,
                              FFT::PEAK,
                              false,
                              fft_proc_config_ );

    if (config_.properties.src_size_write_enabled)
    {
      size_t window_size = fft_proc_config_.getWindowSize();
      Tango::DevLong desired_num_samples = Tango::DevLong(window_size) + config_.skipped_first_samples;
      Tango::DeviceAttribute dev_attr(config_.properties.src_size_attr_name, desired_num_samples);
      dev_proxy_->write_attribute(dev_attr);
    }
   INFO_STREAM << "\tTuneTask::configure_i -> DONE!\n" << std::endl;
  }

  void TuneTask::process_i()
  {
    DEBUG_STREAM << "Processing new data" << ENDLOG;
    yat::Timer timer;

    //- read input
    Tango::DeviceAttribute dev_attr;
    dev_attr = dev_proxy_->read_attribute(config_.properties.src_data_attr_name);

    if (dev_attr.quality == Tango::ATTR_INVALID || dev_attr.is_empty())
    {
      ERROR_STREAM << "TuneTask::process_i::obtained an invalid (or empty) attribute value from data source device - aborting data processing..." <<  std::endl;
      return;
    }

    size_t window_size = fft_proc_config_.getWindowSize();
    size_t desired_num_samples = window_size + static_cast<size_t>(config_.skipped_first_samples);

    std::vector< double > input_data;
    input_data.resize( desired_num_samples );

    size_t data_length = 0;

    switch ( data_attr_info_.data_type )
    {
    case Tango::DEV_UCHAR:
      {
        if (! dev_attr.UCharSeq->length())
        {
          ERROR_STREAM << "TuneTask::process_i::the CORBA sequence doesn't contain any data - aborting data processing..." <<  std::endl;
          return;
        }
        unsigned char* data = dev_attr.UCharSeq->get_buffer();
        data_length = dev_attr.UCharSeq->length();
        if (data_length <= desired_num_samples)
          std::copy( data, data + data_length, input_data.begin() );
      }
      break;
    case Tango::DEV_SHORT:
      {
        if (! dev_attr.ShortSeq->length())
        {
          ERROR_STREAM << "TuneTask::process_i::the CORBA sequence doesn't contain any data - aborting data processing..." <<  std::endl;
          return;
        }
        short* data = dev_attr.ShortSeq->get_buffer();
        data_length = dev_attr.ShortSeq->length();
        if (data_length <= desired_num_samples)
          std::copy( data, data + data_length, input_data.begin() );
      }
      break;
    case Tango::DEV_USHORT:
      {
        if (! dev_attr.UShortSeq->length())
        {
          ERROR_STREAM << "TuneTask::process_i::the CORBA sequence doesn't contain any data - aborting data processing..." <<  std::endl;
          return;
        }
        unsigned short* data = dev_attr.UShortSeq->get_buffer();
        data_length = dev_attr.UShortSeq->length();
        if (data_length <= desired_num_samples)
          std::copy( data, data + data_length, input_data.begin() );
      }
      break;
    case Tango::DEV_LONG:
      {
        if (! dev_attr.LongSeq->length())
        {
          WARN_STREAM << "TuneTask::process_i::the CORBA sequence doesn't contain any data - aborting data processing..." <<  std::endl;
          return;
        }
        long* data = dev_attr.LongSeq->get_buffer();
        data_length = dev_attr.LongSeq->length();
        if (data_length <= desired_num_samples)
          std::copy( data, data + data_length, input_data.begin() );
      }
      break;
    case Tango::DEV_ULONG:
      {
#if (TANGO_VERSION_MAJOR >= 9)
#pragma message "TANGO_VERSION_MAJOR >= 9"
        if (! dev_attr.ULongSeq->length())
#else
        if (! dev_attr.get_ULong_data()->length())
#endif
        {
          ERROR_STREAM << "TuneTask::process_i::the CORBA sequence doesn't contain any data - aborting data processing..." <<  std::endl;
          return;
        }
#if (TANGO_VERSION_MAJOR >= 9)
#pragma message "TANGO_VERSION_MAJOR >= 9"
        unsigned long* data = dev_attr.ULongSeq->get_buffer();
        data_length = dev_attr.ULongSeq->length();
#else
        unsigned long* data = dev_attr.get_ULong_data()->get_buffer();
        data_length = dev_attr.get_ULong_data()->length();
#endif
        if (data_length <= desired_num_samples)
          std::copy( data, data + data_length, input_data.begin() );
      }
      break;
    case Tango::DEV_FLOAT:
      {
        if (! dev_attr.FloatSeq->length())
        {
          ERROR_STREAM << "TuneTask::process_i::the CORBA sequence doesn't contain any data - aborting data processing..." <<  std::endl;
          return;
        }
        float* data = dev_attr.FloatSeq->get_buffer();
        size_t  data_length = dev_attr.FloatSeq->length();
        if (data_length <= desired_num_samples)
          std::copy( data, data + data_length, input_data.begin() );
      }
      break;
    case Tango::DEV_DOUBLE:
      {
        if (! dev_attr.DoubleSeq->length())
        {
          ERROR_STREAM << "TuneTask::process_i::the CORBA sequence doesn't contain any data - aborting data processing..." <<  std::endl;
          return;
        }
        double* data = dev_attr.DoubleSeq->get_buffer();
        data_length = dev_attr.DoubleSeq->length();
        if (data_length <= desired_num_samples)
          std::copy( data, data + data_length, input_data.begin() );
      }
      break;
    default:
      ERROR_STREAM << "TuneTask::process_i::the CORBA sequence contains unsupported data type - aborting data processing..." <<  std::endl;
      return;
      break;
    }

    DEBUG_STREAM << "Reading data : " << data_length << " points" << ENDLOG;


    //- check that sizes are coherent with config_
    if (size_t(dev_attr.dim_x) > desired_num_samples)
    {
      ERROR_STREAM << config_.properties.src_data_attr_name << " is " << dev_attr.dim_x << " long but we expect " << desired_num_samples << " points : cancelling update" << ENDLOG;

      // expose an empty data structure
      this->set_empty_data();

      std::ostringstream oss;
      oss << config_.properties.src_data_attr_name << " is " << dev_attr.dim_x << " long but we expect " << desired_num_samples << " points : cancelling update";

      throw yat::Exception( "DATA_ERROR", oss.str(), "TuneTask::process_i()" );
    }
    else if (size_t(dev_attr.dim_x) < desired_num_samples)
    {
      WARN_STREAM << config_.properties.src_data_attr_name << " is " << dev_attr.dim_x << " long but we expect " << desired_num_samples << " points : automatic zero-padding applied" << ENDLOG;
      for (size_t i = size_t(dev_attr.dim_x); i < desired_num_samples; i++)
      {
        input_data.push_back( 0 );
      }
    }


    //- compute a single FFT
    bool single_fft_ready = false;
    try
    {
      DEBUG_STREAM << "Computing FFT..." << ENDLOG;
      single_fft_ready = fft_proc_.process(&input_data.front() + config_.skipped_first_samples,
                                           input_data.size() - config_.skipped_first_samples);
    }
    catch(std::exception & ex)
    {
      throw ex;
    }
    catch(...)
    {
      throw yat::Exception( "UNKNOWN_ERROR", "Unknown error during FFT computation", "TuneTask::process_i" );
    }

    if (single_fft_ready == true)
    {
      DEBUG_STREAM << "FFT ready" << ENDLOG;

      bool fft_phase_ready = false;
      // fft_phase_proc_.init( FFT::DEGREES, true );
      fft_phase_proc_.init( FFT::DEGREES, false );

      try
      {
        // Compute FFT phase
        DEBUG_STREAM << "Computing FFT phase" << ENDLOG;
        fft_phase_ready = fft_phase_proc_.process( fft_proc_.getData() );
      }
      catch(Exception &)
      {
        throw;
      }
      catch(...)
      {
        throw yat::Exception( "UNKNOWN_ERROR", "Unknown error in phase computation", "TuneTask::process_i" );
      }

      bool average_fft_ready = false;
      //- compute the averaged power
      try
      {
        DEBUG_STREAM << "Averaging FFT" << ENDLOG;
        average_fft_ready = fft_average_proc_.process(fft_proc_.getData());
      }
      catch(Exception &)
      {
        throw;
      }
      catch(...)
      {
        throw yat::Exception( "UNKNOWN_ERROR", "Unknown error during the averaging process", "TuneTask::process_i" );
      }

      if (average_fft_ready == true)
      {
        DEBUG_STREAM << "Averaged FFT ready" << ENDLOG;
        //- unit conversion -> logarithmic axis
        try
        {
          DEBUG_STREAM << "Unit conversion" << ENDLOG;
          fft_unit_converter_.process(fft_average_proc_.getData());
        }
        catch(Exception &)
        {
          throw;
        }
        catch(...)
        {
          throw yat::Exception( "UNKNOWN_ERROR", "Unknown error during unit conversion", "TuneTask::process_i" );
        }

        DEBUG_STREAM << "Unit conversion OK : publishing new result" << ENDLOG;
        const FFT::Spectrum& output = fft_unit_converter_.getData();

        TuneDataP  data;
        data.reset( new TuneData() );
        data->config = config_;
        DEBUG_STREAM << "Nb bins in computed spectrum : " << output.getNbBins() << ENDLOG;
        data->fft_nb_bins = output.getNbBins();
        data->fft_size    = 2 * (data->fft_nb_bins - 1);
        data->fft_abs.capacity( data->fft_nb_bins );
        data->fft_abs.force_length( data->fft_nb_bins );
        data->fft_ord.capacity( data->fft_nb_bins );
        data->fft_ord.force_length( data->fft_nb_bins );

        DEBUG_STREAM << "Before scale_factor" << ENDLOG;
        
        double scale_factor = 0.5 / data->fft_nb_bins;
        for (long i = 0; i < data->fft_nb_bins; i++)
        {
          data->fft_abs[i] =  scale_factor * i;
        }

        DEBUG_STREAM << "After scale_factor" << ENDLOG;

        data->fft_ord = output.getRealData();

        // reading the extracted phase data
        if( fft_phase_ready == true )
        {
          const FFT::Spectrum& phase_spectrum = fft_phase_proc_.getData();
          data->fft_phase.capacity( phase_spectrum.getNbBins() );
          data->fft_phase.force_length( phase_spectrum.getNbBins() );
          data->fft_phase = phase_spectrum.getRealData();
        }
        DEBUG_STREAM << "fft_phase_ready" << ENDLOG;


        size_t start_index = (size_t) ::floor(config_.nu_search_start * output.getFFTSize());
        size_t end_index   = (size_t) ::ceil (config_.nu_search_end   * output.getFFTSize()) + 1;

        //- protection for out of bound value
        end_index = (end_index < output.getNbBins()) ? end_index : output.getNbBins();
        start_index = (start_index < end_index) ? start_index : end_index - 1;
        DEBUG_STREAM << "start_index = " << start_index << " && end_index = " << end_index << ENDLOG;

        double fft_max_value = data->fft_ord[start_index];
        size_t fft_max_index = start_index;

        data->upper_peak_nu = 0.1234;
        data->lower_peak_nu = 5.6789;
        
        //- search the second peak value!
        double fft_2max_value;
        size_t fft_2max_index;

        //- generic case : search the only peak!
        for (size_t i = start_index; i < end_index; i++)
        {
          if (data->fft_ord[i] > fft_max_value)
          {
            fft_max_value = data->fft_ord[i];
            fft_max_index = i;
          }
        }
    
        if ( peaks_proxy_ )
        {
          //- search peaks (at least only 2!)
          std::vector<double> in;  //- data
          std::vector<Tango::DevULong> out;   //- peaks indexes found
          // const Tango::DevVarULongArray* out;

          for(std::size_t idx=start_index; idx < end_index; idx++)
          {
            in.push_back(data->fft_ord[idx]);
          }

          Tango::DeviceData argin;
          Tango::DeviceData argout;
          argin << in;
          //- returns peaks found indexes in ascending sort!!
          argout = peaks_proxy_->command_inout("FindPeaks", argin);
          argout >> out;

          std::size_t nb_peaks = out.size();

          //- no peak!
          if(!nb_peaks)
          {
            INFO_STREAM << "No peaks" << std::endl;

						//return;
          }
          //- one peak found
          else if (nb_peaks == 1)
          {
            data->upper_peak_nu = data->fft_abs[start_index+out[0]];
          }
          //- peak(s) found
          else
          {
            INFO_STREAM << "Found " << out.size() << " peaks" << std::endl;

            std::size_t first_peaks_idx = out[nb_peaks-1];

            //- second peak exists?
            std::size_t snd_peaks_idx = out[nb_peaks-2];

						//- check which idx has the biggest tune value
            if (data->fft_abs[start_index+first_peaks_idx] > data->fft_abs[start_index+snd_peaks_idx])
            {	
            	data->upper_peak_nu = data->fft_abs[start_index+first_peaks_idx];
            	data->lower_peak_nu = data->fft_abs[start_index+snd_peaks_idx];
            }
            else						
            {	
            	data->upper_peak_nu = data->fft_abs[start_index+snd_peaks_idx];
            	data->lower_peak_nu = data->fft_abs[start_index+first_peaks_idx];
            }
          }
        DEBUG_STREAM << "upper_peak_nu =" << data->upper_peak_nu << " && lower = " << data->lower_peak_nu << ENDLOG;
          
        }
        DEBUG_STREAM << "after compute max" << ENDLOG;

        if( !config_.switch_search_two_peak )
        {
          data->nu_raw  = data->fft_abs[fft_max_index];
          data->fft_max = fft_max_value;
        }
        else
        {
          if( config_.select_upper_peak )
          {
            data->nu_raw  = data->upper_peak_nu;
            data->fft_max = fft_max_value;
          }
          else
          {
            data->nu_raw  = data->lower_peak_nu;
            data->fft_max = fft_2max_value;
          }    
        }
        DEBUG_STREAM << "select_upper_peak" << ENDLOG;

        // data->nu_raw  = data->fft_abs[fft_max_index];

        // data->fft_max = fft_max_value;

        if (config_.nu_in_higher_interval)
          data->nu = 1 - data->nu_raw;
        else
          data->nu = data->nu_raw;


        DEBUG_STREAM << "nu = " << data->nu << std::endl;

        data->estim_comput_time = static_cast<Tango::DevLong>(timer.elapsed_msec());
        data->valid = true;

        DEBUG_STREAM << "Updating data..." << ENDLOG;
        this->set_data(data);

         yat::Timestamp now;
         _GET_TIME(now);
         std::string time_str;
         _TIMESTAMP_TO_DATE(now,time_str);
         string new_status="New FFT results computed successfully at " + time_str;
         this->set_state_status( Tango::RUNNING, new_status );

      }
    }
  }
}
